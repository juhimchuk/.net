﻿using Ninject.Modules;
using Generation_habits;
using Generation_habits.Interface;
using Generation_habits.Read_data;

namespace GH.IoC
{
    public class BLNinjectModule:NinjectModule
    {
        public override void Load()
        {
            Bind(typeof(IReadData)).To(typeof(ReadData));
            Bind(typeof(IXMLDeserializer)).To(typeof(XMLDeserializer));
            Bind(typeof(ICheckForNumber)).To(typeof(CheckForNumber));

        }
    }
}
