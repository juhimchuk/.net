﻿
using System.Collections.Generic;

namespace Generation_habits.Interface
{
    public interface IXMLDeserializer
    {
        List<string> XmlDeserializer(int week, int day);
    }
}
