﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_generation
{
    class ShowXML
    {
        public void ShowXml(List<string> habits)
        {
            habits.ForEach(Print);
            Console.WriteLine();
        }
        private static void Print(string s)
        {
            Console.Write(s);
        }
    }
}
