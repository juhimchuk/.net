﻿

namespace Task_generation
{
    class XMLSerializer
    {
        public void XmlSerializer(int week, int day, string habits)
        {
            var ioSerializer = new ManagerXMLSerializer(KernelManager.InitKernel());
            ioSerializer.Serializer(week,day,habits);
        }
    }
}
