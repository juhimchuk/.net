﻿using System;
using System.Collections.Generic;
using Ninject;
using Generation_habits.Interface;

namespace Task_generation
{
    class ManagerXMLSerializer
    {
        private IKernel _kernal;
        private IReadData _readData;
        public ManagerXMLSerializer() { }
        public ManagerXMLSerializer(IKernel kernal)
        {
            _kernal = kernal;
            _readData = _kernal.Get<IReadData>();
        }
        public void Serializer(int week, int day,string habits)
        {
            _readData.ReadDatas(week, day,habits); ;
        }
    }
}
