﻿using System;

namespace Task_generation
{
    public class ReadScreen
    {
        public string Read()
        {
            Console.WriteLine(@"To write in the diary, indicate the week number, day number and list of habits that you did not observe on that day.
To start a note, enter +;
If you want to exit the program enter the -- exit;
View results enter result.");
            string choice = Console.ReadLine();
            
            int week,day;
            string habits;
            switch (choice)
            {
                case "+":
                    ReadData reading = new ReadData();
                    reading.ReadDatas(out habits);
                    ReadDatas read = new ReadDatas();
                    read.ReadData(out week, out day);

                    XMLSerializer serializer = new XMLSerializer();
                    serializer.XmlSerializer(week,day,habits);
                    break;
                case "result":
                    ReadDatas reads = new ReadDatas();
                    reads.ReadData(out week, out day);
                    XMLDeserializer deserializer = new XMLDeserializer();
                    deserializer.XmlDeserializer(week,day);
                    break;
                case "exit":
                    break;
                default:
                    break;
            }
            return choice;
        }
    }


}
