﻿using System;
using System.Collections.Generic;
using Ninject;
using Generation_habits.Interface;

namespace Task_generation
{
    class ManagerXMLDeserializer
    {
        private IKernel _kernal;
        private IXMLDeserializer _deserializer;
        public ManagerXMLDeserializer() { }
        public ManagerXMLDeserializer(IKernel kernal)
        {
            _kernal = kernal;
            _deserializer = _kernal.Get<IXMLDeserializer>();
        }
        public List<string> Deserializer(int week, int day)
        {
            return _deserializer.XmlDeserializer(week,day); ;
        }
    }
}
