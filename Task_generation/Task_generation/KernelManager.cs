﻿using Ninject;
using GH.IoC;

namespace Task_generation
{
    
        static class KernelManager
        {
            public static IKernel InitKernel()
            {
                IKernel kernal = new StandardKernel(new BLNinjectModule());
                return kernal;
            }
        }
    
}
