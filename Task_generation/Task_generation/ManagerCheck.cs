﻿using System;
using System.Collections.Generic;
using Ninject;
using Generation_habits.Interface;

namespace Task_generation
{
    class ManagerCheck
    {
        private IKernel _kernal;
        private ICheckForNumber _check;
        public ManagerCheck() { }
        public ManagerCheck(IKernel kernal)
        {
            _kernal = kernal;
            _check = _kernal.Get<ICheckForNumber>();
        }
        public bool Check(string number)
        {
            return _check.CheckNumber(number) ;
        }
    }
}
