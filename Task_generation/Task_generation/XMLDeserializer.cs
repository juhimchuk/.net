﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_generation
{
    class XMLDeserializer
    {
        public void XmlDeserializer(int week,int day)
        {
            var ioDeserializer = new ManagerXMLDeserializer(KernelManager.InitKernel());
            List<string> note = new List<string>();
            note = ioDeserializer.Deserializer(week,day);
            ShowXML show = new ShowXML();
            show.ShowXml(note);
        }
    }
}
