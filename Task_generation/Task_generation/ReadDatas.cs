﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_generation
{
    class ReadDatas
    {
        public void ReadData(out int week, out int day)
        {
            
            var ioCheck = new ManagerCheck(KernelManager.InitKernel());
         
            string sweek,sday;
            Console.WriteLine("Please enter the data:");
            Console.Write("Week number ");
            sweek = Console.ReadLine();
            do
            {
                if (!ioCheck.Check(sweek))
                {
                    Console.Write("Please enter the number week ");
                    sweek = Console.ReadLine();
                }
               
            }
            while (!ioCheck.Check(sweek));
            week = Convert.ToInt32(sweek);
            Console.WriteLine();
            Console.Write("Day number ");
            sday =Console.ReadLine();
            do
            {
                if (!ioCheck.Check(sday))
                {
                    Console.Write("Please enter the number day ");
                    sday = Console.ReadLine();
                }
               
            }
            while (!ioCheck.Check(sweek));
            day = Convert.ToInt32(sday);
            Console.WriteLine();
            
        }
    }
}
