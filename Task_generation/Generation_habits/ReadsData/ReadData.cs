﻿using System;
using System.Collections.Generic;
using Generation_habits.Interface;

namespace Generation_habits.Read_data
{
    public class ReadData:IReadData
    {
        public void ReadDatas(int week,int day, string habits)
        {
            List<string> listHabits = new List<string>();
            listHabits.AddRange(habits.Split(','));
            Habits habit = new Habits(listHabits);
            Tabel tables = new Tabel(week,day,habit);
            List<Tabel> table = new List<Tabel>();
            table.Add(tables);
            //return table
            XMLSerializer serializer = new XMLSerializer();
            serializer.XmlSerializer(table, week,day);
            

        }
    }
}
