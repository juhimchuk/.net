﻿using System;
using System.Collections.Generic;

namespace Generation_habits
{
    [Serializable]
    public class Habits
    {
        public List<string> Habit { get; set; }

        // стандартный конструктор без параметров
        public Habits()
        { }

        public Habits(List<string> habit)
        {
            Habit=habit;
        }
        public override string ToString()
        {
            string habit="";
            
            for (int i = 0; i < Habit.Count; i++)
                habit += String.Format("{0} ",Convert.ToString(Habit[i]));

            return habit;
        }
    }
}
