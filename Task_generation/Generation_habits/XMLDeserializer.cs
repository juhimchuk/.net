﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Generation_habits.Interface;

namespace Generation_habits
{
    public class XMLDeserializer:IXMLDeserializer
    {
        public List<string> XmlDeserializer(int week,int day)
        {

            string nameFile = String.Format("tabel{0}{1}.xml", week, day);
            List<string> deserial = new List<string>();
            XmlSerializer formatter = new XmlSerializer(typeof(List<Tabel>));
            try {
                using (FileStream fs = new FileStream(nameFile, FileMode.OpenOrCreate, FileAccess.Read))
                {
                    List<Tabel> newTable = (List<Tabel>)formatter.Deserialize(fs);

                    foreach (Tabel p in newTable)
                    {
                        deserial.Add(String.Format("Week: {0}", p.Week));
                        deserial.Add(String.Format(" --- Day: {0}",p.Day));
                        deserial.Add(String.Format(" --- Habits: {0}",p.Habits.ToString()));

                    }

                }
            }
            catch(InvalidOperationException)
            {
                deserial.Add("This day is not yet in the records.");
            }
            
            return deserial;
        }
    }
}
