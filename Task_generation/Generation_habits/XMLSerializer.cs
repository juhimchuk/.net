﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Generation_habits
{
    public class XMLSerializer
    {
        public void XmlSerializer(List<Tabel> table, int week,int day)
        {
            string nameFile = String.Format("tabel{0}{1}.xml", week, day);
            XmlSerializer formatter = new XmlSerializer(typeof(List<Tabel>));

            using (FileStream fs = new FileStream(nameFile, FileMode.Create, FileAccess.Write))
            {
                formatter.Serialize(fs, table);
            }

           
        }
    }
}
