﻿
using Generation_habits.Interface;

namespace Generation_habits
{
    public class CheckForNumber : ICheckForNumber
    {
        public bool CheckNumber(string number)
        {
            int numbers;
            return int.TryParse(number, out numbers);
            
        }
    }
}
