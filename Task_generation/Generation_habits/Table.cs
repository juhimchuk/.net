﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generation_habits
{
    [Serializable]
    public class Tabel
    {
        public int Week { get; set; }
        public int Day { get; set; }
        public Habits Habits { get; set; }
        // стандартный конструктор без параметров
        public Tabel()
        { }

        public Tabel(int week, int day, Habits habit)
        {
            Week = week;
            Day = day;
            Habits = habit;
        }

    }
}
