﻿

namespace CommonInterfase
{
    public interface IAppFunctionality
    {
        string MathOperation(double firstNumber,double secondNumber);
    }
}
