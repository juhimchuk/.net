﻿using CommonInterfase;
using System;
using System.Linq;
using System.Reflection;

namespace BLCalculator
{
    public class ClassRef
    {
        public string GetAction(double firstNumber, double secondNumber)
        {
            string result = null;
            Console.Write("For this operation, you need to install the plugin. Do you want to do this? (yes/no) ");
            string answer = Console.ReadLine();
            if (answer.ToLower() == "yes")
            {
                try
                {
                    Console.Write("Assembly name  ");
                    string assemblyName = Console.ReadLine();
                    Assembly asembly = Assembly.LoadFrom(assemblyName);
                    var theClassTypes = from t in asembly.GetTypes()
                                        where t.IsClass &&
                                        (t.GetInterface("IAppFunctionality") != null)
                                        select t;
                   
                    foreach (Type t in theClassTypes)
                    {
                        IAppFunctionality itfApp = (IAppFunctionality)asembly.CreateInstance(t.FullName, true);
                        result=itfApp.MathOperation(firstNumber,secondNumber);
                    }
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }

            return result;


        }
    }
}
