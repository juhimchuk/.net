﻿using System;
using BLCalculator;

namespace Calculator
{
    class ChooseAction
    {
        public void Choose(double firstNumber, double secondNumber)
        {
            Console.WriteLine("Choose action (+,-,*,/)");
            string choose = Console.ReadLine();
            string answer=null;
           if(choose =="+"|| choose == "-"|| choose == "*"|| choose == "/")
            {
                ClassRef classRef = new ClassRef();
                answer= classRef.GetAction(firstNumber, secondNumber);
            }
            Console.WriteLine(answer);
        }
    }
}
