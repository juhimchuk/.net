﻿using System;
using BLCalculator;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            CheckNumber check = new CheckNumber();
            ChooseAction choose = new ChooseAction();
            bool cheked = false;
            string firstNumber;
            string secondNumber;
            do {
                
                do {
                    Console.WriteLine("Input first number : ");
                    firstNumber = Console.ReadLine();

                    cheked = check.Cheking(firstNumber);
                }
                while (!cheked);
                do {
                    Console.WriteLine("Input second number : ");
                    secondNumber = Console.ReadLine();
                    cheked = check.Cheking(secondNumber);
                }
                while (!cheked);
                double firstNumb = Double.Parse(firstNumber);
                double secondNumb = Double.Parse(secondNumber);
                
                choose.Choose(firstNumb, secondNumb);
                Console.WriteLine("Do you want continue : (yes/no) ");
                firstNumber = Console.ReadLine();
            } while (firstNumber == "yes");
        }
    }
}
