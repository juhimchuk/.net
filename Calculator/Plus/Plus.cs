﻿using System;
using CommonInterfase;

namespace Plus
{
    public class Plus:IAppFunctionality
    {
        public string MathOperation(double firstNumber, double secondNumber)
        {
            double answer = firstNumber + secondNumber;
            string answers = String.Format("{0}+{1} = {2}", firstNumber, secondNumber, answer);
            return answers;
        }
    }
}
