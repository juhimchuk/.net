﻿using System;
using CommonInterfase;
namespace Division
{
    public class Division:IAppFunctionality
    {
        public string MathOperation(double firstNumber, double secondNumber)
        {
            if (secondNumber != 0) { 
            double answer = firstNumber / secondNumber;
                string answers = String.Format("{0}/{1} = {2}", firstNumber, secondNumber,answer);
            return answers;
            }
            return "An error occurred when dividing by 0!";

        }

       
    }
}
