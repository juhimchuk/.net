﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL;

namespace ClassWork
{
	class Program
	{
		static void Main(string[] args)
		{
			GalaxyCollection galaxy = new GalaxyCollection();
            // Добавляем обработчики события.
            galaxy.BeforeAdded += ShowMessage;
            galaxy.Added += ShowMessage;
            galaxy.BeforeEditRawn += ShowMessage;
            galaxy.EditRawn += ShowMessage;
            galaxy.BeforeDelete += ShowMessage;
            galaxy.Delete += ShowMessage;

			Console.Write("Name ");
			string name = Console.ReadLine();
			Console.Write("Years ");
			int years = Convert.ToInt32(Console.ReadLine());
            galaxy.Add(name,years);

			Console.Write("OldName ");
			string oldName = Console.ReadLine();
			Console.Write("Name ");
			name = Console.ReadLine();
			Console.Write("Years ");
			years = Convert.ToInt32(Console.ReadLine());
            galaxy.Edit(oldName,name,years);

			Console.Write("Name ");
			name = Console.ReadLine();
            galaxy.Deleted(name);
			
			Console.ReadLine();
		}
		private static void ShowMessage(object sender, GalaxyEventArgs e)
		{
			Console.WriteLine(e.Message);
		}
	}
}
