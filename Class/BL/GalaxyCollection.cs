﻿
using System.Collections.Generic;

namespace BL
{
    public class GalaxyCollection
    {
		// Объявляем делегат
		public delegate void GalaxyStateHandler(object sender,GalaxyEventArgs e);
		// Событие, возникающее при add
		public event GalaxyStateHandler BeforeAdded;
		public event GalaxyStateHandler Added;
		// Событие, возникающее при edit
		public event GalaxyStateHandler BeforeEditRawn;
		public event GalaxyStateHandler EditRawn;
		// Событие, возникающее при delete
		public event GalaxyStateHandler BeforeDelete;
		public event GalaxyStateHandler Delete;


		List<Galaxy> theGalaxies = new List<Galaxy>();


		public void Add(string name,int years)
		{
			if (BeforeAdded != null)
				BeforeAdded(this, new GalaxyEventArgs("Checking"));
			AddGalaxyRule addRule = new AddGalaxyRule();
			bool someRule = addRule.AddRule(years);
			if (someRule)
			{ 
				theGalaxies.Add(new Galaxy(name, years));
				if (Added != null)
					Added(this, new GalaxyEventArgs("Good add"));
				
			}
			else
			{
				if (Added != null)
					Added(this, new GalaxyEventArgs("Bad add years must be > 1000"));
			
			}
		}
		public void Edit(string oldName,string name, int years)
		{
			EditRule editRule = new EditRule();
			bool someRule=false;
			int index = 0;
			int i = 0;
			int ind = -1;
			
				if (BeforeEditRawn != null)
					BeforeEditRawn(this,new GalaxyEventArgs("Checking"));
				foreach (Galaxy p in theGalaxies)
				{
					if (oldName == p.Name)
					{
						index++;
						
						ind = i;
					}
					i++;
				}
			
			if (index>0)
				{
				theGalaxies.RemoveAt(ind);
				someRule =editRule.EditeRule(ind,years);
				}
				if(someRule)
			{ 
				theGalaxies.Insert(ind,new Galaxy(name, years));
				if (EditRawn != null)
					EditRawn(this,new GalaxyEventArgs("Good add"));
			}
			else
			{
				if (EditRawn != null)
					EditRawn(this,new GalaxyEventArgs("Bad add"));
			}
		}
		public void Deleted(string name)
		{
			if (BeforeDelete != null)
				BeforeDelete(this,new GalaxyEventArgs("Checking"));
			DeleteRule delRule = new DeleteRule();
			bool someRule = false;
			int index = 0;
			int i = 0;
			int ind = -1;
			foreach (Galaxy p in theGalaxies)
			{
				if (name == p.Name)
				{
					index++;
					ind = i;
				}
				i++;
			}
			if (index > 0)
			{
				someRule = delRule.DeleteRules(ind);
			}
			if (someRule)
			{
				theGalaxies.RemoveAt(ind);
				if (Delete != null)
					Delete(this,new GalaxyEventArgs("The galaxy delete"));
			}
			else
			{
				if (Delete != null)
					Delete(this, new GalaxyEventArgs("The galaxy not found"));
			}
		}


	}
}

	