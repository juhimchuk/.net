﻿

namespace BL
{
	public class Galaxy
	{
		public string Name { get; set; }
		public int MegaLightYears { get; set; }
		public Galaxy(string name,int years)
		{
			Name = name;
			MegaLightYears = years;
		}
	}
}
