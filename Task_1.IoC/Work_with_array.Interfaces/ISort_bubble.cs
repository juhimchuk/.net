﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work_with_array.Interfaces
{
    public interface ISort_bubble
    {
        void sort_bubble(List<int> b, string[] words);
    }
}
