﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work_with_array.Interfaces
{
    public interface IRead_array_file
    {
        List<int> read_array_file(string change_input);
    }
}
