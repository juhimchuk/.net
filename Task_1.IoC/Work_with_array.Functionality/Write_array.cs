﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Work_with_array.Interfaces;
using System.IO;

namespace Work_with_array.Functionality
{
    public class Write_array : IWriteArray
    {
        public void writearray(string w, List<string> writ)
        {
            if (w.Remove(0, 1) == "screen")
            {
                for (int i = 0; i < writ.Count; i++)
                { Console.Write(writ[i] + " "); }
                Console.WriteLine();
            }
            else
            {
                FileStream file1 = new FileStream(w.Remove(0, 1), FileMode.Create);
                StreamWriter writer = new StreamWriter(file1);
                for (int i = 0; i < writ.Count; i++)
                    writer.Write(writ[i]);
                writer.Close();
            }
        }
    }
}
