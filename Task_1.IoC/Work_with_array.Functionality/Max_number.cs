﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Work_with_array.Interfaces;

namespace Work_with_array.Functionality
{
    public class Max_number : IMax_number
    {
        Write_array writer = new Write_array();
        // search max number in array
        public void max_number(List<int> b, string[] words)
        {
            List<string> addwr = new List<string>();
            int max = b[0];
            for (int i = 0; i < b.Count; i++)
            {
                if (b[i] > max)
                    max = b[i];
            }
            addwr.Add("The max number in this array is ");
            addwr.Add(Convert.ToString(max));
            writer.writearray(words[2], addwr);

        }
    }
}
