﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Work_with_array.Interfaces;

namespace Work_with_array.Functionality
{
    public class Sort_bubble : ISort_bubble
    {
        Write_array writer = new Write_array();
        public void sort_bubble(List<int> b, string[] words)
        {
            List<string> addwr = new List<string>();
            int m = 0;
            for (int i = 0; i < b.Count; i++)
            {
                for (int j = b.Count - 1; j > i; j--)
                {
                    if (b[j - 1] > b[j])
                    {
                        int x = b[j - 1];
                        b[j - 1] = b[j];
                        b[j] = x;
                    }
                    m = m + 1;
                }
            }
            addwr.Add("Sorted array ");
            for (int i = 0; i < b.Count; i++)
            {
                addwr.Add(Convert.ToString(b[i]));
            }
            addwr.Add("Number of passes sorted by bubble: ");
            addwr.Add(Convert.ToString(m));
            writer.writearray(words[2], addwr);

        }
    }
}
