﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Work_with_array.Interfaces;

namespace Work_with_array.Functionality
{
    public class Read_array_screen : IRead_array
    {
        Chek_array chek = new Chek_array();
        public List<int> read_array(string change_input)
        {
            List<int> ilist = new List<int>();
            List<string> slist = new List<string>();
            Console.Write(@"Please write only the number (2 5 6) ");
            do
            {
                slist.Clear();
                string screen_array = Console.ReadLine();
                slist.AddRange(screen_array.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
                if (!chek.chek_arr(slist))
                {
                    Console.Write(@"Please write only the number (2 5 6) ");
                }
            }
            while (!chek.chek_arr(slist));
            for (int i = 0; i < slist.Count; i++)
            {
                ilist.Add(Convert.ToInt32(slist[i]));
                // Console.WriteLine(pro[i]);
            }
            return ilist;
        }
    }
}
