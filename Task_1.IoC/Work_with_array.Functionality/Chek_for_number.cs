﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Work_with_array.Interfaces;

namespace Work_with_array.Functionality
{
    public class Chek_for_number : IChek_for_number
    {
        public bool chek_number(string num)
        {
            int number;
            bool isInt = int.TryParse(num, out number);
            return isInt;
        }
    }
}
