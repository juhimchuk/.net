﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Work_with_array.Interfaces;

namespace Work_with_array.Functionality
{
    public class Sum_numbers_array : ISum_numbers
    {
        Write_array writer = new Write_array();
        public void sum_numbers(List<int> b, string[] words)
        {
            List<string> addwr = new List<string>();
            int sum = 0;
            for (int i = 0; i < b.Count; i++)
            {
                sum += b[i];
            }
            addwr.Add("The suma in this array is ");
            addwr.Add(Convert.ToString(sum));
            writer.writearray(words[2], addwr);

        }
    }
}
