﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Work_with_array.Interfaces;

namespace Work_with_array.Functionality
{
    public class Search_number : ISearch_number
    {
        Write_array writer = new Write_array();
        public void search_number(List<int> b, string[] words, int n = 0)
        {
            List<string> addwr = new List<string>();
            int m = 0;
            int[] index = new int[b.Count];
            for (int i = 0; i < b.Count; i++)
            {
                if (b[i] == n)
                {
                    index[i] = i + 1;
                    m++;
                }
            }
            if (m > 0)
            {
                addwr.Add("The given number occurs under the subscripts ");
                for (int i = 0; i < index.Length; i++)
                {
                    if (index[i] > 0)
                    {
                        addwr.Add(Convert.ToString(index[i] - 1));
                    }
                }
            }
            else
            {
                addwr.Add("This number does not occur in this array");
            }
            writer.writearray(words[2], addwr);

        }
    }
}
