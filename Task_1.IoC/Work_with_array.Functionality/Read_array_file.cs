﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Work_with_array.Interfaces;
using System.IO;

namespace Work_with_array.Functionality
{
    public class Read_array_file : IRead_array_file
    {
        Chek_array chek = new Chek_array();
        public List<int> read_array_file(string change_input)
        {
            List<int> ilist = new List<int>();
            List<string> slist = new List<string>();
            do
            {
                slist.Clear();
                try
                {
                    int count = File.ReadAllLines(change_input).Length;

                    FileStream file1 = new FileStream(change_input, FileMode.Open);
                    StreamReader reader = new StreamReader(file1);

                    for (int i = 0; i < count; i++)
                    {
                        string read = reader.ReadLine();
                        string[] sarray = read.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int j = 0; j < sarray.Length; j++)
                        {

                            slist.Add(sarray[j]);
                        }
                    }
                    reader.Close();
                }
                catch (FileNotFoundException)
                {
                    Console.Write(@"Please select a file that contains only the number (\d:\\array.txt) ");
                    change_input = Console.ReadLine().Remove(0, 1);
                    slist.Add("try");
                }

                if (!chek.chek_arr(slist))
                {
                    Console.Write(@"Please select a file that contains only the number (\d:\\array.txt) ");
                    change_input = Console.ReadLine().Remove(0, 1);

                }
            }
            while (!chek.chek_arr(slist));
            for (int i = 0; i < slist.Count; i++)
            {
                ilist.Add(Convert.ToInt32(slist[i]));
            }
            return ilist;
        }
    }
}
