﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work_with_array.Functionality
{
    public class Chek_array
    {
        public bool chek_arr(List<string> arr)
        {
            int number;
            bool nub_arr = true;
            for (int i = 0; i < arr.Count; i++)
            {
                if (int.TryParse(arr[i], out number) == false)
                {
                    nub_arr = false;
                }
            }
            return nub_arr;
        }
    }
}
