﻿using Ninject.Modules;
using Work_with_array.Functionality;
using Work_with_array.Interfaces;

namespace Work_with_array.IoC
{
    public class BLNinjectModule : NinjectModule
    {
        public override void Load()
        {

            Bind(typeof(IMax_number)).To(typeof(Max_number));
            Bind(typeof(IMultiple_number)).To(typeof(Multiple_number));
            Bind(typeof(ISearch_number)).To(typeof(Search_number));
            Bind(typeof(ISort_bubble)).To(typeof(Sort_bubble));
            Bind(typeof(ISum_numbers)).To(typeof(Sum_numbers_array));
            Bind(typeof(IRead_array_file)).To(typeof(Read_array_file));
            Bind(typeof(IRead_array)).To(typeof(Read_array_screen));
            Bind(typeof(IChek_for_number)).To(typeof(Chek_for_number));
        }
    }
}
