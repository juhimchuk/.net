﻿using Ninject;
using Work_with_array.IoC;

namespace Task_1.IoC
{
    static class KernelManager
    {
        public static IKernel InitKernel()
        {
            IKernel kernal = new StandardKernel(new BLNinjectModule());
            return kernal;
        }
    }
}
