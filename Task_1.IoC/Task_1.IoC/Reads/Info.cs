﻿using System;

namespace Task_1.IoC.Reads
{
    class Info
    {
        //help
        public void info()
        {
            Console.WriteLine(@"The program works with numeric types.
Please write comand (\methodsort \fileread \filewrite)
where methodsort the array processing algorithm,
fileread a file from which it is necessary to consider format:
\D:\\name.type for read from file  or
\screen for read from screen
filewrite a file in which it is necessary to write down the result:
\D:\\name.type for write in file  or
\screen for write in screen
array processing algorithm:
\srch - search for the desired number in the array;
\sb - sorting bubble;
\max - search max number in array;
\sum - suma elements in array;
\mult - search for elements of a multiple of a given number;
exit - close program.");
        }
    }
}
