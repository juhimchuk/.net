﻿using System.Collections.Generic;

namespace Task_1.IoC.Reads
{
    class Suma
    {
        public void sum(Readarr str, string[] words)
        {
            var iosuma = new InputOutputManager(KernelManager.InitKernel());
            List<int> qwe = str.Read_array(words[1].Remove(0, 1));
            iosuma.SumaArray(qwe, words);

        }
    }
}
