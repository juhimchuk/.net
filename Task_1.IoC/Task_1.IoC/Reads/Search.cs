﻿using System;
using System.Collections.Generic;

namespace Task_1.IoC.Reads
{
    class Search
    {

        //search for the desired number in the array
        public void srch(Readarr str, string[] words, InputOutputManager chek)
        {
            var ioSearch = new InputOutputManager(KernelManager.InitKernel());
            List<int> qwe = str.Read_array(words[1].Remove(0, 1));
            Console.Write("Please enter the number for search ");
            string number = Console.ReadLine();
            int numb = 0;
            do
            {
                if (!chek.Chek_number(number))
                {
                    Console.Write("Please enter the number for search ");
                    number = Console.ReadLine();
                }
                else
                {
                    numb = Convert.ToInt32(number);
                }
            }
            while (!chek.Chek_number(number));
            numb = Convert.ToInt32(number);
            ioSearch.SearchArray(qwe, words, numb);

        }
    }
}
