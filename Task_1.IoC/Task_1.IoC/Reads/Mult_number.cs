﻿using System;
using System.Collections.Generic;

namespace Task_1.IoC.Reads
{
    class Mult_number
    {

        //search for elements of a multiple of a given number
        public void mult(Readarr str, string[] words, InputOutputManager chek)
        {
            var iomult = new InputOutputManager(KernelManager.InitKernel());
            List<int> qwe = str.Read_array(words[1].Remove(0, 1));
            Console.Write("Please enter the number ");
            string number = Console.ReadLine();
            int numb = 0;
            do
            {
                if (!chek.Chek_number(number))
                {
                    Console.Write("Please enter the number ");
                    number = Console.ReadLine();
                }
                else
                {
                    numb = Convert.ToInt32(number);
                }
            }
            while (!chek.Chek_number(number));
            numb = Convert.ToInt32(number);
            iomult.MultipleArray(qwe, words, numb);

        }
    }
}
