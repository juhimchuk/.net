﻿using System.Collections.Generic;

namespace Task_1.IoC.Reads
{
    class Max_number
    {
        // search max number in array
        public void max(Readarr str, string[] words)
        {
            var iomax = new InputOutputManager(KernelManager.InitKernel());
            List<int> qwe = str.Read_array(words[1].Remove(0, 1));
            iomax.MaxArray(qwe, words);
        }
    }
}

