﻿using System.Collections.Generic;

namespace Task_1.IoC
{
    class Readarr
    {
        //Input Method Selection
        /// <summary>
        /// choice how to read the array 
        /// </summary>
        /// <param name="change_input">user choice how to read the array</param>
        /// <returns> return int[]  array</returns>
        public List<int> Read_array(string change_input)
        {
            var ioscreen = new InputOutputManager(KernelManager.InitKernel());
            var iofile = new InputOutputManager(KernelManager.InitKernel());
            List<int> arr = new List<int>();
            switch (change_input)
            {
                case "screen":
                    arr.AddRange(ioscreen.ReadArray_screen(change_input));
                    break;
                default:
                    arr.AddRange(iofile.ReadArray_file(change_input));
                    break;
            }
            return arr;
        }
    }
}
