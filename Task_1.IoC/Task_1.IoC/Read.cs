﻿using System;
using Task_1.IoC.Reads;

namespace Task_1.IoC
{
    class Read
    {
        public void stingread(string s)
        {
            var ioCheking = new InputOutputManager(KernelManager.InitKernel());
            string[] words = s.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            Readarr str = new Readarr();
            Info info = new Info();
            switch (words[0])
            {
                case "/?":
                    info.info();
                    break;
                case "\\srch":
                    Search srch = new Search();
                    srch.srch(str, words, ioCheking);
                    break;
                case "\\sb":
                    Sort_bubble sb = new Sort_bubble();
                    sb.sb(str, words);
                    break;
                case "\\max":
                    Max_number max = new Max_number();
                    max.max(str, words);
                    break;
                case "\\sum":
                    Suma sum = new Suma();
                    sum.sum(str, words);
                    break;
                case "\\mult":
                    Mult_number mult = new Mult_number();
                    mult.mult(str, words, ioCheking);
                    break;
                case "exit":
                    break;
                default:
                    Console.WriteLine("You can only call the following functions");
                    info.info();
                    break;
            }
        }
    }
}