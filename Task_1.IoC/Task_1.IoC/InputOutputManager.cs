﻿using Ninject;
using Work_with_array.Interfaces;
using System.Collections.Generic;

namespace Task_1.IoC
{
    class InputOutputManager
    {
        private IKernel _kernal;
        private IMax_number _max;
        private IMultiple_number _mult;
        private ISearch_number _search;
        private ISort_bubble _sort;
        private ISum_numbers _sum;
        private IChek_for_number _chek;
        private IRead_array _reads;
        private IRead_array_file _readf;
        public InputOutputManager() { }

        public InputOutputManager(IKernel kernal)
        {
            _kernal = kernal;
            // _studentFunctionality = _kernal.Get<IStudentFunctionality>();
        }
        public void MaxArray(List<int> b, string[] words)
        {
            _max = _kernal.Get<IMax_number>();
            _max.max_number(b, words);
        }
        public bool Chek_number(string num)
        {
            // _studentFunctionality = _kernal.Get<IStudentFunctionality>();
            _chek = _kernal.Get<IChek_for_number>();

            return _chek.chek_number(num);
        }
        public void SearchArray(List<int> b, string[] words, int n)
        {
            _search = _kernal.Get<ISearch_number>();
            _search.search_number(b, words, n);
        }
        public void MultipleArray(List<int> b, string[] words, int n)
        {
            _mult = _kernal.Get<IMultiple_number>();
            _mult.multiple_number(b, words, n);
        }
        public void SortArray(List<int> b, string[] words)
        {
            _sort = _kernal.Get<ISort_bubble>();
            _sort.sort_bubble(b, words);
        }
        public void SumaArray(List<int> b, string[] words)
        {
            _sum = _kernal.Get<ISum_numbers>();
            _sum.sum_numbers(b, words);
        }
        public List<int> ReadArray_screen(string input)
        {
            _reads = _kernal.Get<IRead_array>();
            return _reads.read_array(input);
        }
        public List<int> ReadArray_file(string input)
        {
            _readf = _kernal.Get<IRead_array_file>();
            return _readf.read_array_file(input);
        }
    }
}
