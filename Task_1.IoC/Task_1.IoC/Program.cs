﻿using System;

namespace Task_1.IoC
{
    class Program
    {
        static void Main(string[] args)
        {

            string s;
            do
            {
                Console.WriteLine(@"Please write comand (\methodsort \fileread \filewrite) or /? for info. The program works with numeric types.");
                Console.Write("sortarray: ");
                s = Console.ReadLine();
                Read read_file = new Read();
                read_file.stingread(s);
            }
            while (s != "exit");
        }
    }
}

