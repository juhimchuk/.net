﻿using System;
using Ninject.Modules;
using Interface_BL.Task;
using Interface_BL.Category;
using Interface_BL;
using BL_ToDoList;
using BL_ToDoList.Category;
using BL_ToDoList.Task;

namespace IoC_BL
{
    public class BLNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind(typeof(IAnalysisLogins)).To(typeof(AnalysisLogins));
            Bind(typeof(IXMLDeserializer)).To(typeof(XMLDeserializer));
            Bind(typeof(ICheckNull)).To(typeof(CheckNulls));
            Bind(typeof(ICheckNumbers)).To(typeof(CheckNumber));
            Bind(typeof(ILogIns)).To(typeof(LogIns));
            Bind(typeof(ILogUps)).To(typeof(LogUps));
            Bind(typeof(IXMLSerializer)).To(typeof(Serializers));
            Bind(typeof(IAddTasks)).To(typeof(AddTasks));
            Bind(typeof(IAddXmlTasks)).To(typeof(AddXmlTasks));
            Bind(typeof(ICheckDates)).To(typeof(CheckDates));
            Bind(typeof(IDeleteTasks)).To(typeof(DeleteTasks));
            Bind(typeof(IViewTasks)).To(typeof(ViewTasks));
            Bind(typeof(IAddCategories)).To(typeof(AddCategories));
            Bind(typeof(IChangeCategories)).To(typeof(ChangeCategories));
            Bind(typeof(IDeleteCategories)).To(typeof(DeleteCategories));
            Bind(typeof(IViewCategories)).To(typeof(ViewCategories));
        }
    }
}
