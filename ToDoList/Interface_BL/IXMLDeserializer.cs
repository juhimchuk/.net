﻿using System;
using System.Collections.Generic;
namespace Interface_BL
{
    public interface IXMLDeserializer
    {
        List<string> XmlDeserializer(string login, string categoryName, string taskName);
    }
}
