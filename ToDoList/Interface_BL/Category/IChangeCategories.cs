﻿

namespace Interface_BL.Category
{
    public interface IChangeCategories
    {
        void ChangeCategory(string login, string categoryName, string newCategoryName);
    }
}
