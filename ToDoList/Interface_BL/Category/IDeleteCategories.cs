﻿
namespace Interface_BL.Category
{
    public interface IDeleteCategories
    {
        void DeleteCategory(string login, string categoryName);
    }
}
