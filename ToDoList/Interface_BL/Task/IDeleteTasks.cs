﻿

namespace Interface_BL.Task
{
    public interface IDeleteTasks
    {
        void DeleteTask(string login, string categoryName, string taskName);
    }
}
