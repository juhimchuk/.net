﻿

namespace Interface_BL.Task
{
    public interface IAddTasks
    {
        void AddTask(string name, string description, string priority, string status, string dateStart, string dateFinish, string reminder, string comment, string categoryName, string login);
    }
}
