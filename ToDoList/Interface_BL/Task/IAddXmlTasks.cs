﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface_BL.Task
{
    public interface IAddXmlTasks
    {
        void AddXmlTask(string name, string description, string priority, string status, string dateStart, string dateFinish, string reminder, string comment);

    }
}
