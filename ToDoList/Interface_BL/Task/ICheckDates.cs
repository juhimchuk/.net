﻿

namespace Interface_BL.Task
{
    public interface ICheckDates
    {
        bool CheckDate(string date);
    }
}
