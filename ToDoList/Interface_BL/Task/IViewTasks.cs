﻿
using System.IO;

namespace Interface_BL.Task
{
    public interface IViewTasks
    {
        FileInfo[] ViewTask(string login, string categoryName);
    }
}
