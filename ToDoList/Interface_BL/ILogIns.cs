﻿

namespace Interface_BL
{
    public interface ILogIns
    {
        bool LogIn(string login, string password);
    }
}
