﻿
namespace Interface_BL
{
    public interface IXMLSerializer
    {
        void Serializer(string taskName, string description, string priority, string status, string dateStart, string dateFinish, string reminder, string comment, string login, string categoryName);
    }
}
