﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using DL_Interface.Task;

namespace DL_ToDoList.Task
{
    public class AddTasks: IAddTasks
    {
        public bool AddTask(string name, string description, int priority, string status, DateTime dateStart, DateTime dateFinish, int reminder, string comment, string categoryName, string login)
        {
            Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();

            if (xlApp == null)
            {
                return false;
            }


            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            
            xlWorkSheet.Cells[1, 1] = "Name";
            xlWorkSheet.Cells[1, 2] = "Deskription";
            xlWorkSheet.Cells[1, 3] = "Priority";
            xlWorkSheet.Cells[1, 4] = "Status";
            xlWorkSheet.Cells[1, 5] = "Date create";
            xlWorkSheet.Cells[1, 6] = "Date start";
            xlWorkSheet.Cells[1, 7] = "Date finish";
            xlWorkSheet.Cells[1, 8] = "Remind";
            xlWorkSheet.Cells[1, 9] = "Comment";
            xlWorkSheet.Cells[2, 1] = name;
            xlWorkSheet.Cells[2, 2] = description;
            xlWorkSheet.Cells[2, 3] = priority;
            xlWorkSheet.Cells[2, 4] = status;
            xlWorkSheet.Cells[2, 5] = DateTime.Now.ToString();
            xlWorkSheet.Cells[2, 6] = dateStart.ToString();
            xlWorkSheet.Cells[2, 7] = dateFinish.ToString();
            xlWorkSheet.Cells[2, 8] = reminder;
            xlWorkSheet.Cells[2, 9] = comment;

            string adress = String.Format("{0}\\{1}\\{2}.xls", login, categoryName, name);

            xlWorkBook.SaveAs(adress, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);

            return true;

        }
    }
}
