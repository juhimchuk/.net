﻿using System.IO;
using DL_Interface.Task;

namespace DL_ToDoList.Task
{
    public class ViewTasks: IViewTasks
    {
        public FileInfo[] ViewTask(string login,string categoryName)
        {
            string adress = string.Format("{0}\\{1}", login, categoryName);  
            DirectoryInfo dirInfo = new DirectoryInfo(adress);
            if (!dirInfo.Exists)
            {
                dirInfo.GetFiles();
            }
            return dirInfo.GetFiles();
        }
    }
}
