﻿using System;
using System.IO;
using DL_Interface.Task;

namespace DL_ToDoList.Task
{
    public class DeleteTasks: IDeleteTasks
    {
        public void DeleteTask(string login, string categoryName,string taskName)
        {
            string adress = String.Format("{0}\\{1}\\{2}", login, categoryName, taskName);
            FileInfo fileInfo = new FileInfo(adress);

            fileInfo.Delete();

        }
    }
}
