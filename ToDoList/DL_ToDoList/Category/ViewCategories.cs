﻿using System.IO;
using DL_Interface.Category;

namespace DL_ToDoList.Category
{
    public class ViewCategories: IViewCategories
    {
        public DirectoryInfo[] ViewCategory(string login)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(login);
            if (!dirInfo.Exists)
            {
                dirInfo.GetDirectories();
            }
           return dirInfo.GetDirectories();
        }
    }
}
