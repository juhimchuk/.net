﻿using System;
using System.IO;
using DL_Interface.Category;

namespace DL_ToDoList.Category
{
    public class ChangeCategories: IChangeCategories
    {
        public void ChangeCategory(string login, string categoryName, string newCategoryName)
        {
            string adress = String.Format("{0}\\{1}", login,categoryName); 
            string newAdress = String.Format("{0}\\{1}", login, newCategoryName);
            DirectoryInfo dirInfo = new DirectoryInfo(adress);
                dirInfo.MoveTo(newAdress);
            
        }
    }
}
