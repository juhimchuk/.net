﻿using System.IO;
using DL_Interface.Category;

namespace DL_ToDoList.Category
{
    public class NewCategories: INewCategories
    {
        public void NewCategory(string login, string categoryName)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(login);
           
                dirInfo.CreateSubdirectory(categoryName);
            
        }
    }
}
