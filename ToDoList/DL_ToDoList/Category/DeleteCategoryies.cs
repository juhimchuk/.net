﻿using System;
using System.IO;
using DL_Interface.Category;

namespace DL_ToDoList.Category
{
    public class DeleteCategoryies: IDeleteCategories
    {
        public void DeleteCategory(string login, string categoryName)
        {
            string adress = String.Format("{0}\\{1}", login, categoryName);
            DirectoryInfo dirInfo = new DirectoryInfo(adress);
                dirInfo.Delete(true);
            
        }
    }
}
