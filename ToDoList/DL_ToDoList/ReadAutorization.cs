﻿using System;
using System.Collections.Generic;
using System.IO;
using DL_Interface;

namespace DL_ToDoList
{
    public class ReadAutorization: IReadAutorization
    {
        public List<List<string>> ReadFileAutorization()
        {
            var logins = new List<List<string>>();
            var loginPassword =new List<string>();
            using (StreamReader stream = new StreamReader("autorization.txt"))
            {
                while (stream.Peek() >= 0)
                {
                        string line = stream.ReadLine();
                        string[] tokens = line.Split(new Char[] { ' ' });
                        for (int j = 0; j < tokens.Length; j++)
                        {
                            loginPassword.Add(tokens[j]);
                        }
                    logins.Add(loginPassword);
                    }
                }
            return logins;
            }
           
        }
    }
