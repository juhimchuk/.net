﻿using System.Collections.Generic;

namespace DL_Interface
{
    public interface IReadAutorization
    {
        List<List<string>> ReadFileAutorization();
    }
}
