﻿

namespace DL_Interface.Category
{
   public interface IChangeCategories
    {
        void ChangeCategory(string login, string categoryName, string newCategoryName);
    }
}
