﻿

namespace DL_Interface.Category
{
   public interface IDeleteCategories
    {
        void DeleteCategory(string login, string categoryName);
    }
}
