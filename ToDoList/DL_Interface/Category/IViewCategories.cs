﻿
using System.IO;

namespace DL_Interface.Category
{
   public interface IViewCategories
    {
        DirectoryInfo[] ViewCategory(string login);
    }
}
