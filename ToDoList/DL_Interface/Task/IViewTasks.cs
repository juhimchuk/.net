﻿
using System.IO;

namespace DL_Interface.Task
{
   public interface IViewTasks
    {
        FileInfo[] ViewTask(string login, string categoryName);
    }
}
