﻿using System;

namespace DL_Interface.Task
{
    public interface IAddTasks
    {
        bool AddTask(string name, string description, int priority, string status, DateTime dateStart, DateTime dateFinish, int reminder, string comment, string categoryName, string login);
    }
}
