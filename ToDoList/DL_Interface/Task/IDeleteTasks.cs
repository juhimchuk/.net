﻿

namespace DL_Interface.Task
{
   public interface IDeleteTasks
    {
        void DeleteTask(string login, string categoryName, string taskName);
    }
}
