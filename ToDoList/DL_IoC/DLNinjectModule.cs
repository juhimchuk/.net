﻿using System;
using Ninject.Modules;
using DL_Interface;
using DL_Interface.Category;
using DL_Interface.Task;
using DL_ToDoList;
using DL_ToDoList.Category;
using DL_ToDoList.Task;
namespace DL_IoC
{
    public class DLNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind(typeof(INewUsers)).To(typeof(NewUser));
            Bind(typeof(IReadAutorization)).To(typeof(ReadAutorization));
            Bind(typeof(IAddTasks)).To(typeof(AddTasks));
            Bind(typeof(IDeleteTasks)).To(typeof(DeleteTasks));
            Bind(typeof(IViewTasks)).To(typeof(ViewTasks));
            Bind(typeof(IChangeCategories)).To(typeof(ChangeCategories));
            Bind(typeof(IDeleteCategories)).To(typeof(DeleteCategoryies));
            Bind(typeof(INewCategories)).To(typeof(NewCategories));
            Bind(typeof(IViewCategories)).To(typeof(ViewCategories));
        }
    }
}
