﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IoC_BL;

namespace ToDoList
{
    static class KernelManager
    {
        public static IKernel InitKernel()
        {
            IKernel kernal = new StandardKernel(new BLNinjectModule());
            return kernal;
        }
    }
}
