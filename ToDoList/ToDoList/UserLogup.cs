﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoList.Manager;

namespace ToDoList
{
    public class UserLogup
    {
        public void Logup()
        {
            var ioAnalysis = new ManagerAnalysisLogin(KernelManager.InitKernel());
            string login;
            do {
                Console.Write("Enter login - ");
                login = Console.ReadLine();
                Console.Write("Enter password - ");
                string password = Console.ReadLine();
                
                if (ioAnalysis.Analisis(login))
                {
                    var ioLogup = new ManagerLogUp(KernelManager.InitKernel());
                    
                    ioLogup.Logup(login, password);
                }
                else
                {
                    Console.WriteLine("Select another login.");
                }
            }
            while (!ioAnalysis.Analisis(login));
        }
    }
}
