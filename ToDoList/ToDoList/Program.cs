﻿using System;

namespace ToDoList
{
    class Program
    {
        static void Main(string[] args)
        {
            string choice;
            do {
                Console.WriteLine("Enter login for enter or logup for registration or exit for exit");
                Console.Write("Enter action - ");
                choice = Console.ReadLine();
                switch (choice)
                {
                    case "login":
                        var login = new UserLogIns();
                        login.LogIn();
                        break;
                    case "logup":
                        var logup = new UserLogup();
                        logup.Logup();
                        break;
                }
            }
            while (choice!="exit");
        }
    }
}
