﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using ToDoList.Manager;

namespace ToDoList.Category
{
    public class ChangeNameCategories
    {
        public void Change(string login)
        {
            int count = 0;
            var ioManager = new ManagerViewCategory(KernelManager.InitKernel());
           
            var directory = ioManager.ViewCategory(login);
            string categoryName;
            do {
                Console.WriteLine();
                Console.Write("Enter category name - ");
                categoryName = Console.ReadLine();
                Console.Write("Enter new category name - ");
                string newCategoryName = Console.ReadLine();
                foreach (DirectoryInfo dir in directory)
                {
                    if (categoryName == dir.Name)
                    {
                        count++;
                    }
                }
                if  (count!=0) {
                    if (categoryName.ToLower() != newCategoryName.ToLower())
                    {
                        var ioChange = new ManagerChangeCategory(KernelManager.InitKernel());

                        ioChange.ChangeCategory(login, categoryName, newCategoryName);
                        categoryName = "repeat";
                    }
                    else
                    {
                        Console.WriteLine("Enter a name for the category other than the existing one.");
                    }
                }
                else
                {
                    Console.WriteLine("Enter the name of the category that exists.");
                }
                
            }
            while (categoryName!="repeat");
            Console.WriteLine();
            var choice = new ChoiceAction();
            choice.Choice(login);
        }
    }
}
