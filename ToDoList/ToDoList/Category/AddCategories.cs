﻿using System;
using ToDoList.Manager;

namespace ToDoList.Category
{
    public class AddCategories
    {
        public void AddCategory(string login)
        {
            var ioManager = new ManagerCheckNull(KernelManager.InitKernel());
            bool control;
            string categoryName;
            do
            {
                Console.Write("Enter category name - ");
                categoryName = Console.ReadLine();
                control = ioManager.CheckNull(categoryName);
            }
            while (control);
            
            var ioAddCategory = new ManagerIAddCategory(KernelManager.InitKernel());

            ioAddCategory.AddCategory(login,categoryName);
            Console.WriteLine();
            var choice = new ChoiceAction();
            choice.Choice(login);
        }
    }
}
