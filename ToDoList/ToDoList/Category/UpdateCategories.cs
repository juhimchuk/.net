﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoList.Task;
using System.IO;
using ToDoList.Manager;

namespace ToDoList.Category
{
     public class UpdateCategories
    {
        public void UpdateCategory(string login)
        {
            var update = new UpdateCategories();
            string categoryName;
            Console.WriteLine("Choice of action.");
            Console.WriteLine(@"1. View all task.
2. Add new task.
3. Delete task.
4. Update task.
back.");
            string choice = Console.ReadLine();
            do {
                switch (choice)
                {
                    case "1":
                        categoryName = update.ChoiceCategory(login);
                        var view = new ViewTasks();
                        view.ViewTask(login, categoryName);
                        break;
                    case "2":
                        categoryName = update.ChoiceCategory(login);
                        var add = new AddTasks();
                        add.AddTask(categoryName, login);
                        break;
                    case "3":
                        categoryName = update.ChoiceCategory(login);
                        var delete = new DeleteTasks();
                        delete.DeleteTask(login, categoryName);
                        break;
                    case "4":
                        categoryName = update.ChoiceCategory(login);
                        var upd = new AddTasks();
                        upd.AddTask(categoryName, login);
                        break;
                    case "back":
                        var choic = new ChoiceAction();
                        choic.Choice(login);
                        break;
                    default:
                        Console.WriteLine("Select the correct action.");
                        choice = "repeat";
                        break;
                } }
            while (choice == "repeat");
        }
        public string ChoiceCategory(string login)
        {
            int count = 0;
            var ioView = new ManagerViewCategory(KernelManager.InitKernel());
            
            var directory = ioView.ViewCategory(login);
            string categoryName;
            do
            {
                Console.WriteLine();
                Console.Write("Enter category name - ");
                categoryName = Console.ReadLine();
                foreach (DirectoryInfo dir in directory)
                {
                    if (categoryName == dir.Name)
                    {
                        count++;
                    }
                }
                if (count == 0)
                {

                    Console.WriteLine("Enter the name of the category that exists.");
                }
            }
            while (count ==0);
            Console.WriteLine();

            return categoryName;
        }
    }
}
