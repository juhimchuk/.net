﻿using System;
using System.IO;
using ToDoList.Manager;

namespace ToDoList.Category
{
    public class DeleteCategories
    {
        public void DeleteCategory(string login)
        { int count = 0;
            do {

                Console.Write("Enter category name - ");
                string categoryName = Console.ReadLine();
                var ioManeger = new ManagerViewCategory(KernelManager.InitKernel());
                
                var directory = ioManeger.ViewCategory(login);
                foreach (DirectoryInfo dir in directory)
                {
                    if (categoryName == dir.Name)
                    {
                        var ioDelete = new ManagerDeleteCategory(KernelManager.InitKernel());
                        
                        ioDelete.DeleteCategory(login, categoryName);
                        count++;
                    }
                }
            }
            while (count == 0);
            Console.WriteLine();
            var choice = new ChoiceAction();
            choice.Choice(login);
        }
    }
}
