﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoList.Category
{
    public class ChoiceAction
    {
        public void Choice(string login)
        {
            Console.WriteLine("Choice of action.");
            Console.WriteLine(@"1 - View all category.
2 - Add new category.
3 - Delete category.
4 - Update category.
5 - Change name category.
back - back to login.");

            string choice;
            do {
                Console.Write("Enter action - ");
                choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        var view = new ViewCategories();
                        view.ViewCategory(login);
                        break;
                    case "2":
                        var add = new AddCategories();
                        add.AddCategory(login);
                        break;
                    case "3":
                        var delete = new DeleteCategories();
                        delete.DeleteCategory(login);
                        break;
                    case "4":
                        var update = new UpdateCategories();
                        update.UpdateCategory(login);
                        break;
                    case "5":
                        var change = new ChangeNameCategories();
                        change.Change(login);
                        break;
                    case "back":
                        var back = new UserLogIns();
                        back.LogIn();
                        break;
                    default:
                        Console.WriteLine("Select the correct action.");
                        choice = "repeat";
                        break;
                }
            }
            while (choice=="repeat");
        }
    }
}
