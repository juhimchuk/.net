﻿using System;
using System.IO;
using ToDoList.Manager;

namespace ToDoList.Category
{
    public class ViewCategories
    {
      public void ViewCategory(string login)
        {
            var ioManager = new ManagerViewCategory(KernelManager.InitKernel());
            
            var directory = ioManager.ViewCategory(login);
            Console.WriteLine("Category:");
            foreach (DirectoryInfo dir in directory)
                Console.WriteLine(dir.Name);
            Console.WriteLine();
            var choice = new ChoiceAction();
            choice.Choice(login);
        }
    }
}
