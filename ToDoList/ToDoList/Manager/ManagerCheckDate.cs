﻿using Ninject;
using Interface_BL.Task;

namespace ToDoList.Manager
{
   public class ManagerCheckDate
    {
        private IKernel _kernal;
        private ICheckDates _checkDate;
        public ManagerCheckDate() { }
        public ManagerCheckDate(IKernel kernal)
        {
            _kernal = kernal;
            _checkDate = _kernal.Get<ICheckDates>();
        }
        public  bool CheckDate(string date)
        {
            return _checkDate.CheckDate(date);
        }
    }
}
