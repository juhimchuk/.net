﻿using Ninject;
using Interface_BL.Category;
using System.IO;

namespace ToDoList.Manager
{
   public  class ManagerViewCategory
    {
        private IKernel _kernal;
        private IViewCategories _viewCategory;
        public ManagerViewCategory() { }
        public ManagerViewCategory(IKernel kernal)
        {
            _kernal = kernal;
            _viewCategory = _kernal.Get<IViewCategories>();
        }
        public DirectoryInfo[] ViewCategory(string login)
        {
           return _viewCategory.ViewCategory(login);
        }
    }
}
