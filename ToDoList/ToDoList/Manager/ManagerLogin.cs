﻿using Ninject;
using Interface_BL;

namespace ToDoList.Manager
{
   public class ManagerLogin
    {
        private IKernel _kernal;
        private ILogIns _login;
        public ManagerLogin() { }
        public ManagerLogin(IKernel kernal)
        {
            _kernal = kernal;
            _login = _kernal.Get<ILogIns>();
        }
        public bool Login(string login, string password)
        {
            return _login.LogIn(login,password);
        }
    }
}
