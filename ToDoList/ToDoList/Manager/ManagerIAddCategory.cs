﻿using Ninject;
using Interface_BL.Category;

namespace ToDoList.Manager
{
    public class ManagerIAddCategory
    {
        private IKernel _kernal;
        private IAddCategories _addCategory;
        public ManagerIAddCategory() { }
        public ManagerIAddCategory(IKernel kernal)
        {
            _kernal = kernal;
            _addCategory = _kernal.Get<IAddCategories>();
        }
        public void AddCategory(string login, string categoryName)
        {
          _addCategory.AddCategory(login,categoryName);
        }
    }
}
