﻿using Ninject;
using System.Collections.Generic;
using Interface_BL;

namespace ToDoList.Manager
{
    public class ManagerXMLDeserializer
    {
        private IKernel _kernal;
        private IXMLDeserializer _deserializer;
        public ManagerXMLDeserializer() { }
        public ManagerXMLDeserializer(IKernel kernal)
        {
            _kernal = kernal;
            _deserializer = _kernal.Get<IXMLDeserializer>();
        }
        public List<string> Deserializer(string login, string categoryName, string taskName)
        {
           return _deserializer.XmlDeserializer(login,categoryName,taskName);
        }
    }
}
