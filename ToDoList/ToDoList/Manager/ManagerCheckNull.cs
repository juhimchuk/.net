﻿using Ninject;
using Interface_BL;

namespace ToDoList.Manager
{
   public class ManagerCheckNull
    {
        private IKernel _kernal;
        private ICheckNull _checkNull;
        public ManagerCheckNull() { }
        public ManagerCheckNull(IKernel kernal)
        {
            _kernal = kernal;
            _checkNull = _kernal.Get<ICheckNull>();
        }
        public bool CheckNull(string text)
        {
            return _checkNull.CheckNull(text);
        }
    }
}
