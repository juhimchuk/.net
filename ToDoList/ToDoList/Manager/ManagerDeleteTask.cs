﻿using Ninject;
using Interface_BL.Task;

namespace ToDoList.Manager
{
   public class ManagerDeleteTask
    {
        private IKernel _kernal;
        private IDeleteTasks _deleleteTask;
        public ManagerDeleteTask() { }
        public ManagerDeleteTask(IKernel kernal)
        {
            _kernal = kernal;
            _deleleteTask = _kernal.Get<IDeleteTasks>();
        }
        public void DeleteTask(string login, string categoryName, string taskName)
        {
            _deleleteTask.DeleteTask(login,categoryName,taskName);
        }
    }
}
