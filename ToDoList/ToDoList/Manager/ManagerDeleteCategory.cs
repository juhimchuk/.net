﻿using Ninject;
using Interface_BL.Category;

namespace ToDoList.Manager
{
    public class ManagerDeleteCategory
    {
        private IKernel _kernal;
        private IDeleteCategories _deleteCategory;
        public ManagerDeleteCategory() { }
        public ManagerDeleteCategory(IKernel kernal)
        {
            _kernal = kernal;
            _deleteCategory = _kernal.Get<IDeleteCategories>();
        }
        public void DeleteCategory(string login, string categoryName)
        {
            _deleteCategory.DeleteCategory(login, categoryName);
        }
    }
}
