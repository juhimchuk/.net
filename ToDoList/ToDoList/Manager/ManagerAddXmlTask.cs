﻿using Ninject;
using Interface_BL.Task;

namespace ToDoList.Manager
{
   public class ManagerAddXmlTask
    {
        private IKernel _kernal;
        private IAddXmlTasks _addXmlTask;
        public ManagerAddXmlTask() { }
        public ManagerAddXmlTask(IKernel kernal)
        {
            _kernal = kernal;
            _addXmlTask = _kernal.Get<IAddXmlTasks>();
        }
        public void AddTask(string name, string description, string priority, string status, string dateStart, string dateFinish, string reminder, string comment)
        {
            _addXmlTask.AddXmlTask(name, description, priority, status, dateStart, dateFinish, reminder, comment);
        }
    }
}
