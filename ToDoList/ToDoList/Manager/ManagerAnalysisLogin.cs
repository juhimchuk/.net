﻿using Ninject;
using Interface_BL;


namespace ToDoList.Manager
{
    public class ManagerAnalysisLogin
    {
        private IKernel _kernal;
        private IAnalysisLogins _analysis;
        public ManagerAnalysisLogin() { }
        public ManagerAnalysisLogin(IKernel kernal)
        {
            _kernal = kernal;
            _analysis = _kernal.Get<IAnalysisLogins>();
        }
        public bool Analisis(string login)
        {
            return _analysis.Analisis(login);
        }
    }
}
