﻿using Ninject;
using Interface_BL;
namespace ToDoList.Manager
{
    public class ManagerLogUp
    {

        private IKernel _kernal;
        private ILogUps _logup;
        public ManagerLogUp() { }
        public ManagerLogUp(IKernel kernal)
        {
            _kernal = kernal;
            _logup = _kernal.Get<ILogUps>();
        }
        public void Logup(string login, string password)
        {
            _logup.LogUp(login,password);
        }
    }
}
