﻿using Ninject;
using Interface_BL.Task;

namespace ToDoList.Manager
{
   public class ManagerAddTask
    {
        private IKernel _kernal;
        private IAddTasks _addTask;
        public ManagerAddTask() { }
        public ManagerAddTask(IKernel kernal)
        {
            _kernal = kernal;
            _addTask = _kernal.Get<IAddTasks>();
        }
        public void AddTask(string name, string description, string priority, string status, string dateStart, string dateFinish, string reminder, string comment, string categoryName, string login)
        {
            _addTask.AddTask( name, description, priority, status,  dateStart, dateFinish, reminder,  comment,  categoryName,  login);
        }
    }
}
