﻿using Ninject;
using Interface_BL;

namespace ToDoList.Manager
{
    public class ManagerXMLSerializer
    {
        private IKernel _kernal;
        private IXMLSerializer _serializer;
        public ManagerXMLSerializer() { }
        public ManagerXMLSerializer(IKernel kernal)
        {
            _kernal = kernal;
            _serializer = _kernal.Get<IXMLSerializer>();
        }
        public void Serializer(string taskName, string description, string priority, string status, string dateStart, string dateFinish, string remind, string comment, string login, string categoryName)
        {
            _serializer.Serializer(taskName, description, priority, status, dateStart, dateFinish, remind, comment, login,categoryName);
        }
    }
}
