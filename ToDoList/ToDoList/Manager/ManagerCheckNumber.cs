﻿using Ninject;
using Interface_BL;
namespace ToDoList.Manager
{
    public class ManagerCheckNumber
    {
        private IKernel _kernal;
        private ICheckNumbers _checkNumber;
        public ManagerCheckNumber() { }
        public ManagerCheckNumber(IKernel kernal)
        {
            _kernal = kernal;
            _checkNumber = _kernal.Get<ICheckNumbers>();
        }
        public bool Cheking(string number)
        {
            return _checkNumber.Cheking(number);
        }
    }
}
