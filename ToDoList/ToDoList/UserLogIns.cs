﻿using System;
using ToDoList.Category;
using ToDoList.Manager;

namespace ToDoList
{
    public class UserLogIns
    {
        public void LogIn()
        {
            var ioLogin = new ManagerLogin(KernelManager.InitKernel());
            string login, password;
            do
            {
                Console.Write("Enter login - ");
                login = Console.ReadLine();
                if (login != "back")
                {

                    Console.Write("Enter password - ");
                    password = Console.ReadLine();

                    if (ioLogin.Login(login, password))
                    {
                        var choice = new ChoiceAction();
                        choice.Choice(login);
                    }
                    else
                    {

                        Console.WriteLine("Incorrect login or password.");
                        login = "repeat";
                    }


                }
            }
            while (login == "repeat");
            
        }
    }
}
