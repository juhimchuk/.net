﻿using System;
using System.Collections.Generic;
using System.IO;
using ToDoList.Manager;
using ToDoList.Category;

namespace ToDoList.Task
{
    public class ViewTasks
    {
        public void ViewTask(string login,string categoryName)
        {
            var ioView = new ManagerViewTask(KernelManager.InitKernel());
            
            var directory = ioView.ViewTask(login,categoryName);
            Console.WriteLine("Task:");
            foreach (FileInfo dir in directory)
                Console.WriteLine(dir.Name);
            Console.WriteLine();
            Console.WriteLine("Which file to open? - ");
            string task = Console.ReadLine();
            char[] taskCharArray = task.ToCharArray();
            int lengs = taskCharArray.Length;
            switch(taskCharArray[lengs-1])
            {
                case 'l':
                    var ioDeserializer = new ManagerXMLDeserializer(KernelManager.InitKernel());
                    List<string> infoTask = new List<string>();
                    infoTask = ioDeserializer.Deserializer(login, categoryName, task);
                    
                    Console.WriteLine(@"Task name {0}, 
task description {1}, 
task priority {2}, 
task status {3},
date start {4}, 
date finish {5}, 
reminder {6}, 
comment {7} ",infoTask[0],infoTask[1], infoTask[2], infoTask[3], infoTask[4], infoTask[5], infoTask[6], infoTask[7]);
                    break;
                case 's':
                    break;

            }
            var update = new UpdateCategories();
            update.UpdateCategory(login);
        }
    }
}
