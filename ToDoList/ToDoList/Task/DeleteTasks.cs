﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using ToDoList.Category;
using ToDoList.Manager;

namespace ToDoList.Task
{
    public class DeleteTasks
    {
        public void DeleteTask(string login,string categoryName)
        {
            int count=0;
            do
            {
                Console.Write("Enter task name - ");
                string taskName = Console.ReadLine();
                taskName += ".xls";
                var ioView = new ManagerViewTask(KernelManager.InitKernel());
               
                var directory = ioView.ViewTask(login, categoryName);
                foreach (FileInfo dir in directory)
                {
                    if (taskName == dir.Name)
                    {
                        var ioDelete = new ManagerDeleteTask(KernelManager.InitKernel());

                        ioDelete.DeleteTask(login, categoryName, taskName);
                        count++;
                    }
                }
            }
            while (count == 0);
            Console.WriteLine();
            var update = new UpdateCategories();
            update.UpdateCategory(login);
        }
    }
}
