﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToDoList.Category;
using ToDoList.Manager;

namespace ToDoList.Task
{
    public class AddTasks
    {
        public void AddTask(string categoryName,string login)
        {
            string taskName, description, priority, status, dateStart, dateFinish, remind, comment, save;
            var ioCheckNull = new ManagerCheckNull(KernelManager.InitKernel());
            var ioCheckDate = new ManagerCheckDate(KernelManager.InitKernel());
            
            var ioCheckNumber = new ManagerCheckNumber(KernelManager.InitKernel());
            bool control;
            do {
                Console.Write("Enter task name - ");
                 taskName = Console.ReadLine();
                control = ioCheckNull.CheckNull(taskName);
            }
            while (control);
            do
            {
                Console.Write("Enter description - ");
                description = Console.ReadLine();
                control = ioCheckNull.CheckNull(description);
            }
            while (control);
            do
            {
                Console.Write("Enter priority - ");
                priority = Console.ReadLine();
                control = ioCheckNumber.Cheking(priority);
            }
            while (!control);
            do
            {
                Console.Write("Enter status - ");
           status = Console.ReadLine();

                control = ioCheckNull.CheckNull(status);
            }
            while (control);
            do
            {
                Console.Write("Enter date start (d/m/yyyy h:m) - ");
             dateStart = Console.ReadLine();
            
                control = ioCheckDate.CheckDate(dateStart);
            }
            while (!control);
            do
            {
                Console.Write("Enter date finish (d/m/yyyy h/m) - ");
            dateFinish = Console.ReadLine();
                control = ioCheckDate.CheckDate(dateFinish);
            }
            while (!control);
            
            do
            {
                Console.Write("Enter the frequency of the reminders (hours)- ");
                 remind = Console.ReadLine();
                control = ioCheckNumber.Cheking(remind);
            }
            while (!control);
           
                Console.Write("Enter comments - ");
                comment = Console.ReadLine();
            var ioAdd = new ManagerAddTask(KernelManager.InitKernel());
            
            do {
                Console.Write("How to save a file (.xls or .xml) - ");
                 save = Console.ReadLine();
                switch (save)
                {
                    case ".xls":
                        ioAdd.AddTask(taskName, description, priority, status, dateStart, dateFinish, remind, comment, categoryName, login);
                        break;
                    case ".xml":
                        var ioSerializer = new ManagerXMLSerializer(KernelManager.InitKernel());
                        
                        ioSerializer.Serializer(taskName, description, priority, status, dateStart, dateFinish, remind, comment, login,categoryName);
                        break;
                    default:
                        save = "";
                        break;

                }
            }
            while (save=="");
            var update = new UpdateCategories();
            update.UpdateCategory(login);
        }
    }
}