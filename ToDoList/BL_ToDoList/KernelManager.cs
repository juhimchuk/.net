﻿using Ninject;
using DL_IoC;

namespace BL_ToDoList
{
    static class KernelManager
    {
        public static IKernel InitKernel()
        {
            IKernel kernal = new StandardKernel(new DLNinjectModule());
            return kernal;
        }
    }
}
