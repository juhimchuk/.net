﻿
using Interface_BL.Task;
using BL_ToDoList.Manager;

namespace BL_ToDoList.Task
{
    public class DeleteTasks: IDeleteTasks
    {
        public void DeleteTask(string login, string categoryName,string taskName)
        {
            var ioManager = new ManagerDeleteTask(KernelManager.InitKernel());
            
            ioManager.DeleteTask(login, categoryName, taskName);
        }
    }
}
