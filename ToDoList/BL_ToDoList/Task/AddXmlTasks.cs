﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface_BL.Task;

namespace BL_ToDoList.Task
{
    [Serializable]
    public class AddXmlTasks: IAddXmlTasks
    {
        public string Name { get; set; }
        public String Description { get; set; }
        public int Priority { get; set; }
        public String Status { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateFinish { get; set; }
        public int Reminder { get; set; }
        public string Comment { get; set; }
        public AddXmlTasks()
        { }

        public void AddXmlTask(string name, string description, string priority, string status, string dateStart, string dateFinish, string reminder, string comment)
        {
            Name = name;
            Description = description;
            Priority = Convert.ToInt32(priority);
            Status = status;
            DateCreate = DateTime.Now;
            DateStart = Convert.ToDateTime(dateStart);
            DateFinish = Convert.ToDateTime(dateFinish);
            Reminder = Convert.ToInt32(reminder);
            Comment = comment;
        }
    }
}
