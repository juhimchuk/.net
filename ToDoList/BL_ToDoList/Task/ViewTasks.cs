﻿
using System.IO;
using Interface_BL.Task;
using BL_ToDoList.Manager;

namespace BL_ToDoList.Task
{
    public class ViewTasks: IViewTasks
    {
        public FileInfo[] ViewTask(string login, string categoryName)
        {
            var ioManager = new ManagerViewTask(KernelManager.InitKernel());

            return ioManager.ViewTask(login, categoryName);
             
        }
    }
}
