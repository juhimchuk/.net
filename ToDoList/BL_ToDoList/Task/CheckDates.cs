﻿using System;
using Interface_BL.Task;

namespace BL_ToDoList.Task
{
    public class CheckDates:ICheckDates
    {
        public bool CheckDate(string date)
        {
            DateTime dates = new DateTime();
            dates = DateTime.Now;
            return DateTime.TryParse(date,out dates);
           
        }
    }
}
