﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface_BL.Task;
using BL_ToDoList.Manager;

namespace BL_ToDoList.Task
{
    public class AddTasks: IAddTasks
    {
        public void AddTask(string name, string description, string priority, string status, string dateStart, string dateFinish, string reminder, string comment, string categoryName, string login)
          {
            int priorities = Convert.ToInt32(priority);
            int reminders = Convert.ToInt32(reminder);
            DateTime dateStarts = Convert.ToDateTime(dateStart);
            DateTime dateFinishs = Convert.ToDateTime(dateFinish);
            var ioManager = new ManagerAddTask(KernelManager.InitKernel());
            
            ioManager.AddTask( name, description, priorities,status, dateStarts, dateFinishs, reminders, comment, categoryName, login);
        }
    }
}
