﻿
using BL_ToDoList.Task;
using Interface_BL;

namespace BL_ToDoList
{
    public class Serializers:IXMLSerializer
    {
        public void Serializer(string taskName, string description, string priority, string status, string dateStart, string dateFinish, string reminder, string comment, string login, string categoryName)
        {
            AddXmlTasks add = new AddXmlTasks();
            add.AddXmlTask(taskName, description, priority, status, dateStart, dateFinish, reminder, comment);
            XMLSerializer serializer = new XMLSerializer();
            serializer.XmlSerializer(add,login, categoryName, taskName);
        }
    }
}
