﻿using System;
using System.IO;
using System.Xml.Serialization;
using BL_ToDoList.Task;

namespace BL_ToDoList
{
   public class XMLSerializer
    {
        public void XmlSerializer(AddXmlTasks task,string login,string categoryName,string taskName)
        {
            string adress = String.Format("{0}\\{1}\\{2}.xml", login, categoryName, taskName);
            XmlSerializer formatter = new XmlSerializer(typeof(AddXmlTasks));

            using (FileStream fs = new FileStream(adress, FileMode.OpenOrCreate, FileAccess.Write))
            {
                formatter.Serialize(fs, task);
            }


        }
    }
}
