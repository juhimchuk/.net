﻿using BL_ToDoList.Manager;
using Interface_BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_ToDoList
{
    public class AnalysisLogins : IAnalysisLogins
    {
        public bool Analisis(string login)
        {
            var ioManager = new ManagerReadAutorization(KernelManager.InitKernel());
            
            var autorization = ioManager.ReadFileAutorization();
            for (int index = 0; index < autorization.Count; index++)
            {
                if (login == autorization[index][0])
                {
                    return false;
                }

            }
            return true;
        }
    }
}
