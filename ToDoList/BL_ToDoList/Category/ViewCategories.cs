﻿
using Interface_BL.Category;
using System.IO;
using BL_ToDoList.Manager;

namespace BL_ToDoList.Category
{
    public class ViewCategories: IViewCategories
    {
        public DirectoryInfo[] ViewCategory(string login)
        {
            var ioManager = new ManagerViewCategory(KernelManager.InitKernel());
            
            var directory = ioManager.ViewCategory(login);
            return directory;
        }
    }
}
