﻿
using Interface_BL.Category;
using BL_ToDoList.Manager;

namespace BL_ToDoList.Category
{
    public class DeleteCategories: IDeleteCategories
    {
        public void DeleteCategory(string login, string categoryName)
        {
            var ioManager = new ManagerDeleteCategory(KernelManager.InitKernel());
            
            ioManager.DeleteCategory(login, categoryName);
        }
    }
}
