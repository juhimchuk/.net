﻿
using Interface_BL.Category;
using BL_ToDoList.Manager;

namespace BL_ToDoList.Category
{
    public class ChangeCategories: IChangeCategories
    {
        public void ChangeCategory(string login,string categoryName, string newCategoryName)
        {
            var ioManager = new ManagerChangeCategory(KernelManager.InitKernel());
            
            ioManager.ChangeCategory(login, categoryName, newCategoryName);
        }
    }
}
