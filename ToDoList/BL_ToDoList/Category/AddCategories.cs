﻿
using Interface_BL.Category;
using BL_ToDoList.Manager;

namespace BL_ToDoList.Category
{
    public class AddCategories: IAddCategories
    {
        public void AddCategory(string login, string categoryName)
        {
            var ioManager = new ManagerNewCategory(KernelManager.InitKernel());

            ioManager.NewCategory(login, categoryName);
        }
    }
}
