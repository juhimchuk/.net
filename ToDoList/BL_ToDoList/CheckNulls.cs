﻿using System;
using Interface_BL;

namespace BL_ToDoList
{
    public class CheckNulls:ICheckNull
    {
        public bool CheckNull(string text)
        {
            return String.IsNullOrEmpty(text);
           
        }
    }
}
