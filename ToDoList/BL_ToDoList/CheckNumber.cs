﻿using System;
using Interface_BL;

namespace BL_ToDoList
{
    public class CheckNumber:ICheckNumbers
    {
        public bool Cheking(string number)
        {
            int numbers;
            return Int32.TryParse(number, out numbers);
        }
    }
}
