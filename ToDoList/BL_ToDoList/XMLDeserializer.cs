﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Interface_BL;

namespace BL_ToDoList.Task
{
    public class XMLDeserializer: IXMLDeserializer
    {
        public List<string> XmlDeserializer( string login, string categoryName, string taskName)
        {
            string adress = String.Format("{0}\\{1}\\{2}", login, categoryName, taskName);
            XmlSerializer formatter = new XmlSerializer(typeof(AddXmlTasks));
            using (FileStream fs = new FileStream(adress, FileMode.OpenOrCreate, FileAccess.Read))
            {
                AddXmlTasks newTask = (AddXmlTasks)formatter.Deserialize(fs);
                List<string> task = new List<string>();
                
                task.Add(newTask.Name);
                task.Add(newTask.Description);
                task.Add(Convert.ToString(newTask.Priority));
                task.Add(newTask.Status);
                task.Add(Convert.ToString(newTask.DateStart));
                task.Add(Convert.ToString(newTask.DateFinish));
                task.Add(Convert.ToString(newTask.Reminder));
                task.Add(newTask.Comment);
                return task;
                    
                
            }
        }
    }
}
