﻿using Ninject;
using DL_Interface;

namespace BL_ToDoList.Manager
{
   public class ManagerNewUser
    {
        private IKernel _kernal;
        private INewUsers _newUser;
        public ManagerNewUser() { }
        public ManagerNewUser(IKernel kernal)
        {
            _kernal = kernal;
            _newUser = _kernal.Get<INewUsers>();
        }
        public void AddUser(string login, string password)
        {
            _newUser.AddUser(login,password);
        }
    }
}
