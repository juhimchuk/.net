﻿using Ninject;
using DL_Interface.Category;
using System.IO;

namespace BL_ToDoList.Manager
{
    class ManagerNewCategory
    {
        private IKernel _kernal;
        private INewCategories _newCategory;
        public ManagerNewCategory() { }
        public ManagerNewCategory(IKernel kernal)
        {
            _kernal = kernal;
            _newCategory = _kernal.Get<INewCategories>();
        }
        public void NewCategory(string login, string categoryName)
        {
            _newCategory.NewCategory(login, categoryName);
        }
    }
}
