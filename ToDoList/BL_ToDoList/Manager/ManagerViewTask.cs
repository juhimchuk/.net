﻿using Ninject;
using DL_Interface.Task;
using System.IO;

namespace BL_ToDoList.Manager
{
   public class ManagerViewTask
    {
        private IKernel _kernal;
        private IViewTasks _viewTask;
        public ManagerViewTask() { }
        public ManagerViewTask(IKernel kernal)
        {
            _kernal = kernal;
            _viewTask = _kernal.Get<IViewTasks>();
        }
        public FileInfo[] ViewTask(string login, string categoryName)
        {
            return _viewTask.ViewTask(login, categoryName);
        }
    }
}
