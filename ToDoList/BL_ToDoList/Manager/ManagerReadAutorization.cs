﻿using Ninject;
using DL_Interface;
using System.Collections.Generic;

namespace BL_ToDoList.Manager
{
   public class ManagerReadAutorization
    {
        private IKernel _kernal;
        private IReadAutorization _readAutorization;
        public ManagerReadAutorization() { }
        public ManagerReadAutorization(IKernel kernal)
        {
            _kernal = kernal;
            _readAutorization = _kernal.Get<IReadAutorization>();
        }
        public List<List<string>> ReadFileAutorization()
        {
           return _readAutorization.ReadFileAutorization();
        }
    }
}
