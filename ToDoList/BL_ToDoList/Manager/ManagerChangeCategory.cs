﻿using Ninject;
using DL_Interface.Category;
using System.IO;


namespace BL_ToDoList.Manager
{
   public class ManagerChangeCategory
    {
        private IKernel _kernal;
        private IChangeCategories _changeCategory;
        public ManagerChangeCategory() { }
        public ManagerChangeCategory(IKernel kernal)
        {
            _kernal = kernal;
            _changeCategory = _kernal.Get<IChangeCategories>();
        }
        public void ChangeCategory(string login, string categoryName, string newCategoryName)
        {
            _changeCategory.ChangeCategory(login, categoryName, newCategoryName);
        }
    }
}
