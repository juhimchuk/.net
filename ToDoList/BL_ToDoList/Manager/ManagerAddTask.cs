﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using DL_Interface.Task;
using System.IO;

namespace BL_ToDoList.Manager
{
    class ManagerAddTask
    {
        private IKernel _kernal;
        private IAddTasks _addTask;
        public ManagerAddTask() { }
        public ManagerAddTask(IKernel kernal)
        {
            _kernal = kernal;
            _addTask = _kernal.Get<IAddTasks>();
        }
        public bool AddTask(string name, string description, int priority, string status, DateTime dateStart, DateTime dateFinish, int reminder, string comment, string categoryName, string login)
        {
           return _addTask.AddTask(name, description, priority,status, dateStart, dateFinish, reminder,comment, categoryName, login);
        }
    }
}
