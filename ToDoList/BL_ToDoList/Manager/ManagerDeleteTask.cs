﻿using Ninject;
using DL_Interface.Task;
using System.IO;

namespace BL_ToDoList.Manager
{
   public class ManagerDeleteTask
    {
        private IKernel _kernal;
        private IDeleteTasks _deleteTask;
        public ManagerDeleteTask() { }
        public ManagerDeleteTask(IKernel kernal)
        {
            _kernal = kernal;
            _deleteTask = _kernal.Get<IDeleteTasks>();
        }
        public void DeleteTask(string login, string categoryName, string taskName)
        {
            _deleteTask.DeleteTask(login, categoryName, taskName);
        }
    }
}
