﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface_BL;
using BL_ToDoList.Manager;

namespace BL_ToDoList
{
    public class LogIns: ILogIns
    {
        public bool LogIn(string login,string password)
        {
            var ioManager = new ManagerReadAutorization(KernelManager.InitKernel());
            var autorization = ioManager.ReadFileAutorization();
            for(int index=0;index<autorization.Count;index++)
            {
                for (int indexLogin = 1; indexLogin < autorization[index].Count; indexLogin++)
                {
                   
                    if (login == autorization[index][indexLogin-1])
                    {
                        if (password == autorization[index][indexLogin])
                        {
                            return true;
                        }
                    }
                    indexLogin++;
                }
                
            }
            return false;
        }
    }
}
